# -*- coding: utf-8 -*-
"""
Main script

In this script you should create the main structure of your speaker detector

Created on Mon Oct 24 20:40:40 2022

@author: ValBaron10
"""

from joblib import load

# Get the array information and read the data you want to process

    
# Create the computation grid you want to work on

    
# Compute the transfer function between the source and the mics



# Localize the source



# Obtain a focused signal in the direction of arrival



# Compute the features on this signal



    
# Load the scaler and SVM model to test the class of your source
scaler = load("SCALER")
model = load("SVM_MODEL")


# Get the class of your source
prediction = model.predict(FEATURES)


# Analyze the results
    
