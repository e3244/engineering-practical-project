# Engineering project SICOM 2023

## Goal of the project

Your goal is to build a small software to localize an acoustic source, and to distinguish if it is a human or a white noise.
At the end of the 4 hours, you should provide a script that can be launched using the provided data.

## Organization

This practical work has to be done in teams of 5/6. Once your team is created you should follow the following planning if you want to be able to provide something at the end of the session:

0. Fork the provided repository in somebody personal Gitlab account (create one if necessary), to be able to push/pull on it (in advance)
1. Clone the code in each of your computer, read and understand it (30')
2. Define all the tasks that you think necessary to deliver the product, and assign them to team members (30')
3. Each subteam works on its task, to achieve what is needed (2h)
4. Group all what has been developed together. Use Gitlab to merge all your code together (30')
5. Each group does a demo for the rest of the class (5' each group)

## Be careful

You will work on a project with a very limited deadline. It is easy to loose your way during the 4 hours. Try to exchange very regularly with all the subteams to be sure that two people are not working on the same tasks.