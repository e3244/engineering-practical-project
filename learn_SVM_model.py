# -*- coding: utf-8 -*-
"""
Script to learn a model with Scikit-learn.

Created on Mon Oct 24 20:51:47 2022

@author: ValBaron10
"""

from sklearn import preprocessing
from sklearn import svm
from joblib import dump
from features_functions import compute_features



# LOOP OVER THE SIGNALS
#for all signals:
    # Get an input signal
    input_sig = # Import a signal
    
    
    # Compute the signal in three domains
    sig_sq = input_sig**2
    sig_t = focused_sig / np.sqrt(sig_sq.sum())
    sig_f = np.absolute(np.fft.fft(sig_t))
    sig_c = np.absolute(np.fft.fft(sig_f))
    
    # Compute the features and store them
    features_list = []
    N_feat, features_list = compute_features(sig_t, sig_f[:sig_t.shape[0]//2], sig_c[:sig_t.shape[0]//2], fs=25600)
    features_vector = np.array(features_list)[np.newaxis,:]


# Store the obtained features in a np.arrays
learningFeatures = # 2D np.array with feature values in it

# Store the labels
learningLabels = # np.array with labels in it



# Encode the class names
labelEncoder = preprocessing.LabelEncoder().fit(learningLabels) # To add the "Gros Drone" class during learning stage to avoid a crash in test phase
learningLabelsStd = labelEncoder.transform(learningLabels)

# Learn the model
model = svm.SVC(C=10, kernel='linear', class_weight=None, probability=False)
scaler = preprocessing.StandardScaler(with_mean=True).fit(learningFeatures)
learningFeatures_scaled = scaler.transform(learningFeatures)
model.fit(learningFeatures_scaled, learningLabelsStd)

# Export the scaler and model
dump(scaler, "SCALER")
dump(model, "SVM_MODEL")

